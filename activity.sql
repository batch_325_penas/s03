--database blog_db

--users

INSERT INTO users (email, password, datetime_created) VALUES('johnsmith@gmail.com', 'passwordA', '2021-01-01 1:00:00');
INSERT INTO users (email, password, datetime_created) VALUES('juandelacruz@gmail.com', 'passwordB', '2021-01-01 2:00:00');
INSERT INTO users (email, password, datetime_created) VALUES('janesmith@gmail.com', 'passwordC', '2021-01-01 3:00:00');
INSERT INTO users (email, password, datetime_created) VALUES('mariadelacruz@gmail.com', 'passwordD', '2021-01-01 4:00:00');
INSERT INTO users (email, password, datetime_created) VALUES('johndoe@gmail.com', 'passwordE', '2021-01-01 5:00:00');


--posts

INSERT INTO posts (autor_id, title, content, datetime_posted) VALUES(1, 'First Code', 'Hello World!', '2021-01-01 1:00:00');
INSERT INTO posts (title, content, datetime_posted) VALUES('Second Code', 'Hello Earth!', '2021-01-01 2:00:00');
INSERT INTO posts (title, content, datetime_posted) VALUES('Third Code', 'Welcome to Mars!', '2021-01-01 3:00:00');
INSERT INTO posts (title, content, datetime_posted) VALUES('Fourth Code', 'Bye bye Solar System!', '2021-01-01 4:00:00');

--GET posts author ID 1

SELECT * FROM posts WHERE user_id = 1;

-- Get all the user's email and datetime of creation
SELECT email, datetime_created FROM users;

-- Update a post's content to "Hello to the people of the Earth!" where its initial content is "Hello Earth!" by using the record's ID.
UPDATE posts SET content = 'Hello to the people of the Earth!' WHERE content = 'Hello Earth!';

-- Delete the user with an email of "johndoe@gmail.com"
DELETE FROM users WHERE email = "johndoe@gmail.com";
